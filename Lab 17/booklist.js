$(document).ready(function () {
  // --------- jQuery Data Section ---------
  var book1 = {
    title: "Seveneves",
    author: "Neal Stephenson"
  };
  var book2 = {
    title: "How Not to be Wrong",
    author: "Jordan Ellenberg"
  };
  var book3 = {
    title: "The Vital Question",
    author: "Nick Lane"
  };
  var book4 = {
    title: "The Power to Compete",
    author: "Ryoichi Mikitani and Hiroshi Mikitani"
  };
  var book5 = {
    title: "Sapiens: A Brief History of Humankind",
    author: "Noah Yaval Harai"
  };
  var books = new Array;
  books.push(book1);
  books.push(book2);
  books.push(book3);
  books.push(book4);
  books.push(book5);

 var img_ref = {
   url: 'http://www.gannett-cdn.com/-mm-/dcc982e8ed28edbe24141c93759ed7448c9e2642/c=58-0-965-682&r=x404&c=534x401/local/-/media/2016/05/20/USATODAY/USATODAY/635993645233133294-EPA-USA-WORLD-BANK-IMF-SPRING-MEETINGS-81292935.JPG',
   src: 'http://bit.ly/1RiIMVl',
   alt: 'Bill Gates',
   height: 100, // orig 401 by 4:1
   width: 133 // orig 534 by 4:1
 };
   var reference = {
   url: 'http://www.usatoday.com/story/money/markets/2016/05/20/bill-gates-read-these-5-books-summer/84675556/',
   src: 'http://usat.ly/20hirO3',
   alt: 'Gates Books',
   text: 'BG:5 Books for Summer 2016'}
  // --------- jQuery Data Section ---------
  // --------- jQuery Code Section ---------
  var el1 = document.getElementById("runBook1");
    el1.innerHTML = "1. " + "\"" + book1.title + "\"" + " by " + book1.author;
  var el2 = document.getElementById("runBook2");
    el2.innerHTML = "2. " + "\"" + book2.title + "\"" + " by " + book1.author;
  var el3 = document.getElementById("runBook3");
    el3.innerHTML = "3. " + "\"" + book3.title + "\"" + " by " + book1.author;
  var el4 = document.getElementById("runBook4");
    el4.innerHTML = "4. " + "\"" + book4.title + "\"" + " by " + book1.author;
  var el5 = document.getElementById("runBook5");
    el5.innerHTML = "5. " + "\"" + book5.title + "\"" + " by " + book1.author;
})
$('ol').css ({
  padding: "10px"
});
$('#runBook2, #runBook4').css( {
    padding: "5px",
    background:"lightgrey",
  });
$('#runBook1, #runBook2, #runBook3, #runBook4, #runBook5').css( {
    border: "1px lightgrey solid",
    padding: "5px",
});
$('body').css( {
    border: "1px white solid"
});
