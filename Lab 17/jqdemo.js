$(document).ready(function () {
  // add a 5px red dashed boarder around the panel div
  // provide a 10 pixel padding around all of the divs
    $('.panel').css( {
    border: "5px dashed red",
    padding: "10px",
    });
  // set all div's padding to 3px
  // set all divs in the panel to background gray
  // set all divs of class cat to green
  $('.cat').css( {
    color: "green",
    padding: "3px",
    background:"gray"
  });
  // set all div's padding to 3px
  // set all divs in the panel to background gray
  // set all divs of class dog to red
  $('.dog').css( {
    padding: "3px",
    color:"red",
    background: "gray"
  });
  // set the id of lab to a 1px solid yellow border
  $('#lab').css( {
     border: "1px solid yellow"
  });
  // set the last div with class dog to background yellow
  $('div.dog:last').css( {
    background: "yellow"
  });
  // set the calico cat's width to 50%,
  // background to green and color to white});
  $('div.cat:last').css( {
    width: "50%",
    background:"green",
    color: "white"
  });
})
