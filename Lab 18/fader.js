$(document).ready(function(){
    $(".p1").click(function(){
        $("#outE1").fadeOut("slow")
    });
      $(".p2").click(function(){
        $("#outE2").fadeOut("fast")
    });
   $(".p3").click(function(){
        $("#outE3").hide("slow", function() {})
    });
      $(".p4").click(function(){
        $("#inE1").fadeOut("slow")
    });
      $(".p5").click(function(){
        $("#inE2").fadeOut("fast")
    });
   $(".p6").click(function(){
        $("#inE3").hide("slow", function() {})
    });
    $(".p4").click(function(){
        $("#inE1").fadeIn("slow")
    });
    $(".p5").click(function(){
        $("#inE2").fadeIn("fast")
    });
    $(".p6").click(function(){
        $("#inE3").fadeIn("fast")
    });
    $(".p7").click(function(){
        $("#toE1").fadeTo(100, 0.8)
    });
    $(".p8").click(function(){
        $("#toE2").fadeTo(500, 0.6)
    });
    $(".p9").click(function(){
        $("#toE3").fadeTo(1000, 0.4)
    });
    $(".p10").click(function(){
      $("#toggleE1").fadeToggle("slow", "swing");
    });
    $(".p11").click(function(){
      $("#toggleE2").fadeToggle(1500);
    });
    $(".p12").click(function(){
      $("#toggleE3").fadeToggle(3000);
    });
});
