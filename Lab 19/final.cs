body, #button {
  padding: 10px;
  font-family:'Century Gothic', CenturyGothic, AppleGothic, sans-serif;
  text-transform:uppercase;
}
#header {
  text-align: center;
  font-size: 300%;
  font-family:'Century Gothic', CenturyGothic, AppleGothic, sans-serif;
  border: 2px dashed black;
}
button {
  width: 100%;
}
ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333;
}
li a {
    display: block;
    color: white;
    text-align: center;
    padding: 2px;
    text-decoration: none;
    font-family: "Century Gothic", CenturyGothic, AppleGothic, sans-serif;
}
li a:hover {
    background-color: lightgray;
}
