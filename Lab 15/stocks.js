var stocks = new Array;

var el = document.getElementById('headerTable');
el.innerHTML = "<tr><td>Company Name</td><td>Market Cap</td><td>Sales</td><td>Profit</td><td>Number of Employees</td></tr>";

function myFunction(item, index) {
  var el = document.getElementById('dataTable');
el.innerHTML += "<tr><td>" + item.name + "</td><td>" + item.marketCap + "</td><td>" + item.sales + "</td><td>" + item.profit + "</td><td>" + item.employees + "</td></tr>";
}

stocks.push(
  {
  name: "Microsoft",
  marketCap: "$381.7 B",
  sales: "$86.8 B",
  profit: "$26.7 B",
  employees: "128,000"
  }
);
stocks.push(
  {
  name: "Symetra Financial",
  marketCap: "$2.7 B",
  sales: "$2.2 B",
  profit: "$978.3 M",
  employees: "1,400"
  }
);
stocks.push(
  {
  name: "Micron Technology",
  marketCap: "$37.6 B",
  sales: "$16.4 B",
  profit: "$3.0 B",
  employees: "30,400"
  }
);
stocks.push(
  {
  name: "F5 Networks",
  marketCap: "$9.5 B",
  sales: "$1.7 B",
  profit: "$526.3 M",
  employees: "4,460"
  }
)
stocks.push(
  {
  name: "Expedia",
  marketCap: "$10.8 B",
  sales: "$5.8 B",
  profit: "$1.0 B",
  employees: "$20,000"
  }
);
stocks.push(
  {
  name: "Nautilus",
  marketCap: "$476 M",
  sales: "$274.4 M",
  profit: "$31.2 M",
  employees: "311"
  }
);
stocks.push(
  {
  name: "Heritage Financial",
  marketCap: "$531 M",
  sales: "$137.6 M",
  profit: "$47.4 M",
  employees: "748"
  }
);
stocks.push(
  {
  name: "Cascade Microtech",
  marketCap: "$239 M",
  sales: "$136 M",
  profit: "$19.3 M",
  employees: "401"
  }
);
stocks.push(
  {
  name: "Nike",
  marketCap: "$83.1 B",
  sales: "$27.8 B",
  profit: "$2.1 B",
  employees: "62,600"
  }
);
stocks.push(
  {
  name: "Alaska Air Group",
  marketCap: "$7.9 B",
  sales: "$5.4 B",
  profit: "$336 M",
  employees: "11,536"
  }
);
