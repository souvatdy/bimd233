var division = ['Washington', 'Washington State', 'Standford', 'California', 'Oregon', 'Oregon State'];
var conference = ['7-1', '7-1', '6-3', '2-6', '2-6', '2-6'];
var overall = ['10-1', '8-3', '8-3', '4-7', '4-7', '3-8'];

function myFunction() {
  var one = document.querySelectorAll('li#one');
  if (one.length > 0) {
    for (var i = 0; i < one.length; i++)
      one[i].id = 'uw';
  }
  var two = document.querySelectorAll('li#two');
  if (two.length > 0) {
    for (var i = 0; i < two.length; i++)
      two[i].id = 'wsu';
  }
  var three = document.querySelectorAll('li#three');
  if (three.length > 0) {
    for (var i = 0; i < three.length; i++)
      three[i].id = 'su';
  }
  var four = document.querySelectorAll('li#four');
  if (four.length > 0) {
    for (var i = 0; i < four.length; i++)
      four[i].id = 'cal';
  }
  var five = document.querySelectorAll('li#five');
  if (five.length > 0) {
    for (var i = 0; i < five.length; i++)
      five[i].id = 'ou';
  }
  var six = document.querySelectorAll('li#six');
  if (six.length > 0) {
    for (var i = 0; i < six.length; i++)
      six[i].id = 'osu';
  }
  var el1 = document.getElementById('uw');
  el1.innerHTML =
    "<tr><td>" + division[0] + " " +
    "</td><td>" + conference[0] + " " +
    "</td><td>" + overall[0];
  var el2 = document.getElementById('wsu');
  el2.innerHTML =
    "<tr><td>" + division[1] + " " +
    "</td><td>" + conference[1] + " " +
    "</td><td>" + overall[1];
  var el3 = document.getElementById('su');
  el3.innerHTML =
    "<tr><td>" + division[2] + " " +
    "</td><td>" + conference[2] + " " +
    "</td><td>" + overall[2];
  var el4 = document.getElementById('cal');
  el4.innerHTML =
    "<tr><td>" + division[3] + " " +
    "</td><td>" + conference[3] + " " +
    "</td><td>"+  overall[3];
  var el5 = document.getElementById('ou');
  el5.innerHTML =
    "<tr><td>" + division[4] + " " +
    "</td><td>" + conference[4] + " " +
    "</td><td>" + overall[4];
  var el6 = document.getElementById('osu');
  el6.innerHTML =
    "<tr><td>" + division[5] + " " +
    "</td><td>" + conference[5] + " " +
    "</td><td>" + overall[5];
}
