var image_cell_template = "<div class='col-sm-3 image-cell'><div class='nasa-image'><img></div><div class='image-caption'></div><div class='image-coords'></div></div>";
var date_tmpl = "begin=2016-01-01";
var myApiKey = "api_key=3rW6fM1QORarpUCJOaLaEqV6sjgigNIfwMCy24Pl";

var example_image_url = "https://api.nasa.gov/EPIC/archive/natural/2015/06/13/png/epic_1b_20150613110250_01.png?api_key=3rW6fM1QORarpUCJOaLaEqV6sjgigNIfwMCy24Pl";
var epic_natural_archive_base = "https://api.nasa.gov/EPIC/archive/natural/";
var api_url_query_base = "http://epic.gsfc.nasa.gov/api/natural/date/";

// ==========================================================
// START JS: synchronize the javascript to the DOM loading
// ==========================================================
$(document).ready(function() {
  console.log("ready!");

  // ========================================================
  // SECTION 1:  process on click events
  // ========================================================
  $('#get-images-btn').on('click', api_search);

  // process the "future" img element dynamically generated
  $("div").on("click", "img", function() {
    console.log('img:', this.src);
    // call render_highres() and display the high resolution
  });

  // ========================================================
  // TASK 1:  build the search AJAX call on NASA EPIC
  // ========================================================
  // Do the actual search in this function
  function api_search(e) {

    // get the value of the input search text box => date
    var date = '2016-01-01';

    // build an info object to hold the search term and API key
    var info = {};
    var date_array = date.split('-');
    info.year = ["2016"];
    info.month = ["01"];
    info.day = ["01"];
    info.api_key = ["3rW6fM1QORarpUCJOaLaEqV6sjgigNIfwMCy24Pl"];

    // build the search url and sling it into the URL request HTML element
    var search_url = "https://epic.gsfc.nasa.gov/api/natural/date/2016-01-01?api_key=3rW6fM1QORarpUCJOaLaEqV6sjgigNIfwMCy24Pl";
    console.log(search_url);
    // sling it!

    // make the jQuery AJAX call!
    $.ajax({
      url: search_url,
      success: function(data) {
        render_images(data, info);
      },
      cache: false
    });
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================

  // ========================================================
  // TASK 2: perform all image grid rendering operations
  // ========================================================
  function render_images(data, info) {
    if ((i % 4) === 0) {}
    $(rowselector).append(image_cell_template);
    // get NASA earth data from search results data
    console.log(data.length);
    var images = ['images']['caption'];
    var dates = ['date']['time'];
    var location = ['lat']['lon'];

    var str = "<li>" + images + "," + dates + "</li>";
    $('ul').append(str);

    for (var i = 0; i < data.length; i++) {
      // build an array of objects that captures all of the key image data
      // => image url
      // centroid coordinates to be displayed in the caption area
      // image date to be displayed in the caption area (under thumbnail)
    }

    // select the image grid and clear out the previous render (if any)
    var earth_dom = $('#image-grid');
    earth_dom.empty();

    // render all images in an iterative loop here!
  }

  // ========================================================
  // TASK 3: perform single high resolution rendering
  // ========================================================
  // function to render the high resolution image
  function render_highres(src_url) {
    // use jQuery to select and maniupate the portion of the DOM => #image-grid
    //  to insert your high resolution image
  }
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================
});
