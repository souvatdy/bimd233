var forecast = [
  {date:"May 13", day: "Friday", condition: "Sunny", hi: 82, lo: 55 },
  { date:"May 14", day: "Saturday", condition: "Cloudy", hi: 75, lo: 52 },
  {date:"May 15",  day: "Sunday", condition: "Showers", hi: 69, lo: 52 },
  { date:"May 16", day: "Monday", condition: "Cloudy", hi: 69, lo: 48 },
  {date:"May17",  day: "Tuesday", condition: "Showers", hi: 68, lo: 51 }
];

var high = [82,75,69,69,68];
var low = [55,52,52,48,51];
var highTemp = document.getElementById('high');
var lowTemp = document.getElementById('low');

function getSum(total, num){
  return total + num;
}

function totalAvg(item) {
  document.getElementById("totalAvg").innerHTML = numbers.reduce(getSum);
}

highTemp.innerHTML = " Average High:"+ " " + (high.reduce(getSum)) / high.length + "&#8457;";
lowTemp.innerHTML = "Average Low:"+ " "+ (low.reduce(getSum)) / low.length  +"&#8457;";

var avgZero = ((forecast[0]["hi"] + forecast[0]["lo"])/2)
var avgOne = ((forecast[1]["hi"] + forecast[1]["lo"])/2)
var avgTwo = ((forecast[2]["hi"] + forecast[2]["lo"])/2)
var avgThree = ((forecast[3]["hi"] + forecast[3]["lo"])/2)
var avgFour = ((forecast[4]["hi"] + forecast[4]["lo"])/2)

var el = document.getElementById("dataTable");
el.innerHTML = "<th>Date</th><th>Weekday</th><th>Condition</th><th>Temp (Hi)</th><th>Temp (Lo)</th> <th>Average Temperature</th>";

var i = 0
el.innerHTML +=
  "<tr><td>" + forecast[i]["date"] +
  "</td><td>" + forecast[i]["day"] + "</td><td>" + forecast[i]["condition"] +
  "</td><td>"+ forecast[i]["hi"]+"&#8457;"+
  "</td><td>"+ forecast[i]["lo"]+"&#8457;" +
  "</td><td>"+ avgZero +"&#8457;"+ "</td><tr>";

var i = 1
el.innerHTML +=
 "<tr><td>" + forecast[i]["date"]+
  "</td><td>" + forecast[i]["day"] + "</td><td>" + forecast[i]["condition"] +
  "</td><td>"+ forecast[i]["hi"] +"&#8457;"+
  "</td><td>"+ forecast[i]["lo"] +"&#8457;"+
  "</td><td>"+ avgOne + "&#8457;"+"</td><tr>";

var i = 2
el.innerHTML +=
  "<tr><td>" + forecast[i]["date"]+
  "</td><td>" + forecast[i]["day"] + "</td><td>" + forecast[i]["condition"] +
  "</td><td>"+ forecast[i]["hi"] +"&#8457;"+
  "</td><td>"+ forecast[i]["lo"] +"&#8457;"+
  "</td><td>"+ avgTwo +"&#8457;"+ "</td><tr>";

var i = 3
el.innerHTML +=
  "<tr><td>" + forecast[i]["date"]+
  "</td><td>" + forecast[i]["day"] + "</td><td>" + forecast[i]["condition"] +
  "</td><td>"+ forecast[i]["hi"] +"&#8457;"+
  "</td><td>"+ forecast[i]["lo"]+"&#8457;" +
  "</td><td>"+ avgThree +"&#8457;"+ "</td><tr>";

var i = 4
el.innerHTML +=
  "<tr><td>" + forecast[i]["date"]+
  "</td><td>" + forecast[i]["day"] + "</td><td>" + forecast[i]["condition"] +
  "</td><td>"+ forecast[i]["hi"] +"&#8457;"+
  "</td><td>"+ forecast[i]["lo"] +"&#8457;"+
  "</td><td>"+ avgFour +"&#8457;"+ "</td><tr>";
