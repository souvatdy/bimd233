function onTheFly (airline, number, origin, destination, dep_time, arrival_time, arrival_gate) {
  this.airline = airline;
  this.number = number;
  this.origin = origin;
  this.destination = destination;
  this.dep_time = dep_time;
  this.arrival_time = arrival_time;
  this.arrival_gate = arrival_gate;
  this.flightDuration = function() {
    return this.arrival_time - this.dep_time;
  }
}


var flightOne = new onTheFly ('Alaska Airlines', 'NLA3296', 'Anchorage', 'Seattle', '03:13', '05:24', 5);
var flightTwo = new onTheFly ('Alaska Airlines', 'N24GN', 'Atka', 'Seattle', '12:24', '02:51', 10);
var flightThree = new onTheFly ('Alaska Airlines', 'GUN900', 'Seattle', 'Cold Bay', '03:13', '05:24', 2);
var flightFour = new onTheFly ('Alaska Airlines', 'AER65', 'Sand Point', 'Seattle', '11:30', '12:24', 15);
var flightFive = new onTheFly ('Alaska Airlines', 'AER961', 'Seattle', 'Anchorage', '11:06', '01:51', 9);
var flights = [flightOne, flightTwo, flightThree, flightFour, flightFive];

var el = document.getElementById('table_data');
el.innerHTML = "<th>Airline</th> <th>Number</th> <th>Origin</th> <th>Destination</th> <th>Departure Time</th> <th>Arrival Time</th> <th>Arrival Gate</th>"

var i = 0;
el.innerHTML += "<tr><td>" + flights[i]['airline'] + "</td><td>" + flights[i]['number'] + "</td><td>" + flights[i]['origin'] + "</td><td>" + flights[i]['destination'] + "</td><td>" + flights[i]['dep_time'] + "</td><td>" + flights[i]['arrival_time'] + "</td><td>" + flights[i]['arrival_gate']+ "</td><td>" + flights[i].flightDuration() + "</td></tr>";
var i = 1;
el.innerHTML += "<tr><td>" + flights[i]['airline'] + "</td><td>" + flights[i]['number'] + "</td><td>" + flights[i]['origin'] + "</td><td>" + flights[i]['destination'] + "</td><td>" + flights[i]['dep_time'] + "</td><td>" + flights[i]['arrival_time'] + "</td><td>" + flights[i]['arrival_gate']+ "</td><td>" + flights[i].flightDuration() + "</td></tr>";
var i = 2;
el.innerHTML += "<tr><td>" + flights[i]['airline'] + "</td><td>" + flights[i]['number'] + "</td><td>" + flights[i]['origin'] + "</td><td>" + flights[i]['destination'] + "</td><td>" + flights[i]['dep_time'] + "</td><td>" + flights[i]['arrival_time'] + "</td><td>" + flights[i]['arrival_gate']+ "</td><td>" + flights[i].flightDuration() + "</td></tr>";
var i = 3;
el.innerHTML += "<tr><td>" + flights[i]['airline'] + "</td><td>" + flights[i]['number'] + "</td><td>" + flights[i]['origin'] + "</td><td>" + flights[i]['destination'] + "</td><td>" + flights[i]['dep_time'] + "</td><td>" + flights[i]['arrival_time'] + "</td><td>" + flights[i]['arrival_gate']+ "</td><td>" + flights[i].flightDuration() + "</td></tr>";
var i = 4;
el.innerHTML += "<tr><td>" + flights[i]['airline'] + "</td><td>" + flights[i]['number'] + "</td><td>" + flights[i]['origin'] + "</td><td>" + flights[i]['destination'] + "</td><td>" + flights[i]['dep_time'] + "</td><td>" + flights[i]['arrival_time'] + "</td><td>" + flights[i]['arrival_gate']+ "</td><td>" + flights[i].flightDuration() + "</td></tr>";
var i = 5;
el.innerHTML += "<tr><td>" + flights[i]['airline'] + "</td><td>" + flights[i]['number'] + "</td><td>" + flights[i]['origin'] + "</td><td>" + flights[i]['destination'] + "</td><td>" + flights[i]['dep_time'] + "</td><td>" + flights[i]['arrival_time'] + "</td><td>" + flights[i]['arrival_gate']+ "</td><td>" + flights[i].flightDuration() + "</td></tr>";
