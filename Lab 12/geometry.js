function calcCircleGeometries(radius) {
    const pi = Math.PI;
    var area = pi * radius * radius;
    var circumference = 2 * pi * radius;
    var diameter = 2 * radius;
    var geometries = [area, circumference, diameter];
    return geometries;
}
document.getElementById("radius1").innerHTML = calcCircleGeometries(5);
document.getElementById("radius2").innerHTML = calcCircleGeometries(10);
document.getElementById("radius3").innerHTML = calcCircleGeometries(15);
